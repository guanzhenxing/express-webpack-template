## express-webpack-template

a Node.js express with webpack project template

### Overview
- use express 4.*
- use ES6/ES2015 =>https://babeljs.io
- use connect-flash => https://github.com/jaredhanson/connect-flash
- use express-session for session
- use supervisor for live reload
- use generic-pool as database connections pool
- use cors => https://github.com/expressjs/cors
- use morgan for HTTP request logger middleware
- use es6-promise for  promise/A+
- use multer for upload =>https://github.com/expressjs/multer
- use lodash instead of underscore
- use pm2 for deploy
- use log4js => https://github.com/nomiddlename/log4js-node


#### todo list
- add mongoose
- add test (mocha + chai + sinon)
- add fetch => https://github.com/github/fetch
- ...

### Directory Structure
. <br/>
│──  bin (for executable file) <br/>
│    └── webpack (for webpack config) <br/>
│    └── www (server file)<br/>
│──  config (for config file) <br/>
│──  utils (for utils ) <br/>
│──  middleware (for middleware) <br/>
│──  models (for persistence module <br/>
│──  public (for static file:css/image/js) <br/>
│──  routes (just for routes) <br/>
│──  services (for business logic) <br/>
│──  test (for test) <br/>
│──  utils (for util) <br/>
│──  views (for view engine ) <br/>
│──  app.js (main file) <br/>
│──  webpack.config.js (webpack config file) <br/>

### Install and Running

1. `git clone https://github.com/guanzhenxing/express-webpack-template.git`
2. `cd express-webpack-template`
3. `npm install`
4. `npm install supervisor -g`
5. `npm start`
6. `navigate to http://localhost:3000 in your browser of choice.`

for compile : `npm run compile`

for production : `npm run prod`

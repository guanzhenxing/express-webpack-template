let express = require('express'),
    router = express.Router();

import {writeJsonError} from '../utils';
import OrgServer from '../services/org';
import _ from 'lodash';


router.get('/:id', function (req, res) {
    let id = req.params.id;
    let org = {};
    OrgServer.get(id, function (error, doc) {
        if (error) {
            return writeJsonError(res, error);
        }
        _.assign(org, doc);
        return res.json(org);
    });
});

router.get('/', function (req, res) {

    let param = req.query;


    if (param.page) {
        param.page = parseInt(param.page) || 1;
    }
    if (param.size) {
        param.size = parseInt(param.size) || 0;
    }

    let list = {
        items: [],
        pagination: {
            current: 1,  //当前页
            total: 0,   //数据总数
            pageSize: 20 //每页大小
        }
    }
    OrgServer.list(param, function (err, doc) {
        if (err) {
            return writeJsonError(res, error);
        }
        _.assign(list, doc);
        return res.json(list);
    });

});

router.post('/', function (req, res) {


    let org = {};
    return res.json(org);
});

router.put('/:id', function (req, res) {

    let org = {};
    return res.json(org);
});

router.delete('/:id', function (req, res) {


    let org = {};
    return res.json(org);
});

module.exports = router;
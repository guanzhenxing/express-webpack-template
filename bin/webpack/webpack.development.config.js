var webpack = require('webpack');
var path = require('path');

var publicPath = 'http://localhost:3000/';
var hotMiddlewareScript = 'webpack-hot-middleware/client?reload=true';

var devConfig = {
    entry: {
        home: ['./public/home', hotMiddlewareScript],
        about: ['./public/about', hotMiddlewareScript]
    },
    output: {
        filename: './[name]/bundle.js',
        path: path.resolve('./dist'),
        publicPath: publicPath
    },
    devtool: 'eval-source-map',
    module: {
        loaders: [{
            test: /\.js$/,
            loaders: ['babel?presets[]=es2015,presets[]=stage-0,cacheDirectory=true'],
            exclude: /node_modules/
        },{
            test: /\.(svg|png|jpg|jpeg|gif)$/,
            loader: 'url?limit=8192&context=client&name=[path][name].[ext]'
        },{
            test: /\.css$/,
            loader: 'style!css?sourceMap!resolve-url'
        }, {
            test: /\.scss$/,
            loader: 'style!css?sourceMap!resolve-url!sass?sourceMap'
        }]
    },
    plugins: [
        new webpack.optimize.OccurenceOrderPlugin(),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoErrorsPlugin()
    ]
};

module.exports = devConfig;

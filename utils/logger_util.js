var log4js = require('log4js');
var config = require('../config'); //引入配置文件

var log_config_file = config.log.app.log_config_file;
log4js.configure(log_config_file, {reloadSecs: 300});


module.exports = function (loggerCategoryName = "default") {
    return log4js.getLogger(loggerCategoryName);
}

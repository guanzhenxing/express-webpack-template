/**
 * 开发环境配置信息。
 * 这些配置必须都存在（至少key存在）
 */
module.exports = {
    mongodb: {
        secret: 'lite_admin@github.com',
        host: 'localhost',
        port: '27017',
        db: 'lite_admin',
        url: 'mongodb://localhost/lite_admin'
    },
    session: {
        resave: true,
        saveUninitialized: true,
        secret: "lite_admin",
        key: "lite_admin",
        cookie: {
            secure: false,
            maxAge: 1000 * 60 * 60 * 24 * 30  //30天
        }
    },
    log: {
        access: {
            log_format: 'combined',
            logDirectory: '/logs',
            filename: '/access-%DATE%.log',
            date_formate: 'YYYYMMDD',
            frequency: 'daily',
            verbose: false  //详细的
        },
        app: {
            log_config_file: __dirname + '/log4js/dev.json'
        }
    }
};

import BaseService from '../base';
//import * as PromiseUtil from '../../utils/promise_util';
import {promiseUtil} from '../../utils';


class HomeService extends BaseService {


    print() {

        var timeout = function (callback, ms) {
            return new Promise(function (resolve, reject) {
                setTimeout(function () {
                    resolve(callback());
                }, ms);
            });
        };
        var a1 = function () {
            return new Promise(function (resolve, reject) {
                setTimeout(function () {
                    resolve(1);
                }, 100);
            });
        }
        var a2 = function () {
            return timeout(function () {
                return 2
            }, 100)
        }
        var taskList = [
            a1, a2,
            function () {
                return timeout(function () {
                    return 3
                }, 100)
            }
        ];
        console.time("elapsed time - seq");
        promiseUtil.runInSequence(taskList).then(function (res) {
            console.timeEnd("elapsed time - seq");
            console.log(res);
        })


        console.time("elapsed time - all");
        promiseUtil.runInConcurrent(taskList).then(function (res) {
            console.timeEnd("elapsed time - all");
            console.log(res);
        })


        console.time("elapsed time - limit");
        promiseUtil.runInMaxConcurrent(taskList, 2).then(function (res) {
            console.timeEnd("elapsed time - limit");
            console.log(res);
        })

    }


}
export default new HomeService();